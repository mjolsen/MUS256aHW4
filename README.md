# Spherical Visualizer - MUS256a, HW #4

## Description

This program is a visualizer that features a blue sphere whose parameters are modulated by difference qualities of the incoming audio. The radius of the sphere is controlled by the RMS amplitude value of the incoming audio. The displacement of the individual triangles is determined by the average numerical difference between the samples of the left and right audio channels (i.e. for mono audio, there would be no displacement). The brightness of the color of the sphere is controlled by the spectral centroid of the incoming audio. The rate at which the three different values change is controlled by smoothing filters whose amount of smoothing can be changed by the user.

## Build Instructions
* Extract this directory and place it in your `$OF/apps/myapps/` directory, where `$OF` is the path to your OpenFrameworks directory.

* Download the [ofxFft](https://github.com/kylemcdonald/ofxFft/archive/master.zip) addon and unzip it into `$OF/addons/ofxFft/`. Note that it must have that exact directory name, *not* `ofxFft-master` or anything else.

* Build the program.
    * OSX is untested, but you should be able to import the project into xcode and build from there.
    * On Linux, you can type `make` from the project directory.
    * Windows is untested, but you should be able to import the project into Visual Studio or QtCreator after you've followed the general OpenFrameworks installation directions.

* The compiled program should now be in your `MUS256aHW4/bin` directory.

---

Implemented by Michael Olsen (mjolsenATccrmaDOTstanfordDOTedu) for Music 256a / CS 476a (fall 2016).
