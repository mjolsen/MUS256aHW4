#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    tauValues[0] = 0.001;
    tauValues[1] = 0.333;
    tauValues[2] = 0.667;
    tauValues[3] = 0.999;
    rmsFiltTau = spectCentroidFiltTau = stereoDiffFiltTau = 0;
    curRms = filtRms = 0.0;
    curSpectCentroid = filtSpectCentroid = 0.0;
    curStereoDiff = filtStereoDiff = 0.0;
    prevReal = prevImaginary = realDiff = imaginaryDiff = 0.0;
    rmsFilt.setSmooth(tauValues[rmsFiltTau]);
    curLineWidth = 1.0;
    filtLineWidth = 1.0;
    spectCentroidFilt.setSmooth(tauValues[spectCentroidFiltTau]);
    stereoDiffFilt.setSmooth(tauValues[stereoDiffFiltTau]);
    lineWidthFilt.setSmooth(0.75);
    bInfoText = true;
    ofSetVerticalSync(true);
    ofBackground(20);
    
    // setup the sound stream
    soundStream.setup(this, 0, MY_CHANNELS, MY_SRATE, MY_BUFFERSIZE, MY_NBUFFERS);

    // Resize and initialize left and right audio buffers
    left.resize( MY_BUFFERSIZE, 0 );
    right.resize( MY_BUFFERSIZE, 0 );

    // Setup the FFT
    fft = ofxFft::create(MY_BUFFERSIZE, OF_FFT_WINDOW_HAMMING);

    fftAmps = new float[MY_BUFFERSIZE];
    fftReal = new float[MY_BUFFERSIZE];
    fftImaginary = new float[MY_BUFFERSIZE];

    icoSphere.setRadius(0.03125*ofGetWindowHeight());
    ofSetSphereResolution(24);
}

void ofApp::exit(){
    delete [] fftAmps;
    delete [] fftReal;
    delete [] fftImaginary;
}

//--------------------------------------------------------------
void ofApp::update(){

    filtRms = rmsFilt.tick(curRms); // update filtered RMS value
    filtStereoDiff = stereoDiffFilt.tick(curStereoDiff);

    // Take the FFT of the left channel
    fft->setSignal(&left[0]);

    // Store the different FFT data values
    fftAmps = fft->getAmplitude();
    fftReal = fft->getReal();
    fftImaginary = fft->getImaginary();

    // Normalize FFT magnitudes based on bin length
    bool foundNan = false;
    int fftLength = fft->getBinSize();
    for (int i = 0; i < fftLength; i++){
        if(isnan(fftAmps[i])) 
            foundNan = true;
        else 
            fftAmps[i] = fftAmps[i] / fftLength;
    }
    // if none of the FFT values were Nan, compute the spectral centroid
    if (!foundNan){
        float tempNum = 0.0;
        float tempDenom = 0.0;
        float tempReal = 0.0;
        float tempImaginary = 0.0;
        for (int i = 0; i < fftLength; i++){
            tempNum += (MY_SRATE/(2*fftLength))*(i+1)*abs(fftAmps[i]);
            tempDenom += abs(fftAmps[i]);
            tempReal += fftReal[i];
            tempImaginary += fftImaginary[i];
        }
        tempReal = tempReal/fftLength;
        realDiff = tempReal - prevReal;
        prevReal = tempReal;
        tempImaginary = tempImaginary/fftLength;
        imaginaryDiff = tempImaginary - prevImaginary;
        prevImaginary = tempImaginary;
        if (tempDenom == 0.0)
            curSpectCentroid = 0.0;
        else
            curSpectCentroid = tempNum/tempDenom;
    }
    else{ // just make it zero
        curSpectCentroid = 0.0;
    }

    // compute new filtered spectral centroid value
    filtSpectCentroid = spectCentroidFilt.tick(curSpectCentroid);
    // set random line width and compute new filtered line width value
    curLineWidth = ofRandom(0.5,2.5);
    filtLineWidth = lineWidthFilt.tick(curLineWidth);
}

//--------------------------------------------------------------
void ofApp::draw(){
    // set x/y rotation amounts based on difference between real/imaginary average values
    float spinX = sin(realDiff);
    float spinY = cos(imaginaryDiff);

    int wh = ofGetWindowHeight();
    int ww = ofGetWindowWidth();
    // center icoSphere in middle of screen
    float cy = wh*0.5;
    float cx = ww*0.5;

    ofEnableDepthTest();

    //icoSphere.setResolution(1);
    
    icoSphere.setPosition(cx,cy,0);
    icoSphere.setRadius(1.125*wh*filtRms+0.03125*wh);
    //icoSphere.setRadius(0.875*wh*filtRms+0.03125*wh);
    icoSphere.rotate(spinX,1.0,0.0,0.0);
    icoSphere.rotate(spinY,0.0,1.0,0.0);
    triangles = icoSphere.getMesh().getUniqueFaces();
    ofFill();

    ofColor c = ofColor(0);
    ofVec3f faceNormal;
    for (size_t i = 0; i < triangles.size(); i++ ){
        c.setHsb(153,255,std::max(1,std::min((int)filtSpectCentroid*255/5000,255)));
        faceNormal = triangles[i].getFaceNormal();
        for (int j = 0; j < 3; j++){
                triangles[i].setVertex(j, triangles[i].getVertex(j)+faceNormal*50*filtStereoDiff);
            triangles[i].setColor(j,c);
        }
    }
    icoSphere.getMesh().setFromTriangles( triangles );

    ofSetLineWidth(filtLineWidth);
    ofNoFill();
    icoSphere.setScale(1.01f);
    icoSphere.drawWireframe();
    icoSphere.setScale(1.f);

    if(bInfoText) {
        stringstream ss;
        ss << "(f): Toggle Fullscreen"<<endl;
        ss <<"(1/2): Set Resolutions" <<endl;
        ss <<"(a): Change Amplitude Filter Smoothness " <<tauValues[rmsFiltTau]<<endl;
        ss <<"(s): Change Spectral Centroid Filter Smoothness " <<tauValues[spectCentroidFiltTau]<<endl;
        ss <<"(d): Change Stereo Difference Filter Smoothness " <<tauValues[stereoDiffFiltTau]<<endl;
        ss <<"(t): Info Text"<<endl;
                      
        ofDrawBitmapString(ss.str().c_str(), 20, 20);
    }    
}

//--------------------------------------------------------------
void ofApp::audioIn(float * input, int bufferSize, int nChannels){
    float rmsLeft = 0.0, rmsRight = 0.0, stereoDiff = 0.0;

    // Write incoming audio to buffer. Note: samples are interleaved.
    for (int i = 0; i < bufferSize; i++){
        left[i]		= input[i*2];
        rmsLeft += left[i]*left[i];
        right[i]	= input[i*2+1];
        rmsRight += right[i]*right[i];
        stereoDiff += (left[i]-right[i])*(left[i]-right[i]);
    }
    rmsLeft = std::sqrt(rmsLeft/bufferSize);
    rmsRight = std::sqrt(rmsRight/bufferSize);
    curRms = (0.5)*(rmsLeft+rmsRight);
    curStereoDiff = std::sqrt(stereoDiff);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    switch(key){
        case 'f':
            ofToggleFullscreen();
            break;
        case '1':
            icoSphere.setResolution(0);
            break;
        case '2':
            icoSphere.setResolution(1);
            break;
        case 'a':
            rmsFiltTau += 1;
            if (rmsFiltTau > 3)
                rmsFiltTau = 0;
            rmsFilt.setSmooth(tauValues[rmsFiltTau]);
            break;
        case 's':
            spectCentroidFiltTau += 1;
            if (spectCentroidFiltTau > 3)
                spectCentroidFiltTau = 0;
            spectCentroidFilt.setSmooth(tauValues[spectCentroidFiltTau]);
            break;
        case 'd':
            stereoDiffFiltTau += 1;
            if (stereoDiffFiltTau > 3)
                stereoDiffFiltTau = 0;
            stereoDiffFilt.setSmooth(tauValues[stereoDiffFiltTau]);
            break;
        case 't':
            bInfoText = !bInfoText;
            break;
    }

    //icoSphere.setResolution(icoSphere.getResolution());
    icoSphere.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
    icoSphere.mapTexCoords(0,0,5,5);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
