#pragma once

#include "ofMain.h"
#include "ofxFft.h"

// Preprocessor definitions
#define MY_SRATE    	 48000	// sample rate
#define MY_CHANNELS 	 2      // number of channels
//#define MY_BUFFERHISTORY 50	// number of buffers to save
#define MY_BUFFERSIZE	 1024	// size of a buffer
#define MY_NBUFFERS	 2	// number of buffers
//#define MY_PIE		 3.14159265358979

class Smooth{
private:
    float delay,s;
public:
    Smooth():delay(0.0),s(0.0){}
    ~Smooth(){}

    // set the smoothing (pole)
    void setSmooth(float smooth){
        s = smooth;
    }

    float getValue(){
	return delay;
    }

    // compute one sample
    float tick(float input){
        float currentSample = input*(1.0-s) + delay;
        delay = currentSample*s;
        return currentSample;
    }
};

class ofApp : public ofBaseApp{

public:
    void setup();
    void update();
    void draw();
    void exit() override;

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    void audioIn(float * input, int bufferSize, int nChannels);

private:
    // A sound stream object
    ofSoundStream soundStream;

    // Vectors to hold left/right channel samples
    vector<float> left, right;

    // filters to smooth different values
    // ints hold index of tau value to use from tauValues
    Smooth rmsFilt;
    int rmsFiltTau;
    Smooth spectCentroidFilt;
    int spectCentroidFiltTau;
    Smooth lineWidthFilt;
    Smooth stereoDiffFilt;
    int stereoDiffFiltTau;
    // array of tau values to be used by filters
    float tauValues[4];
    // FFT object
    ofxFft* fft;

    // bool to determine whether to show help text
    bool bInfoText;
    // An icosphere
    ofIcoSpherePrimitive icoSphere;
    // vector of faces
    vector<ofMeshFace> triangles;
    // floats to hold different values relating to audio input
    float curRms, filtRms, curSpectCentroid, filtSpectCentroid, curStereoDiff, filtStereoDiff;
    float prevReal, prevImaginary, realDiff, imaginaryDiff;
    float* fftAmps;
    float* fftReal; 
    float* fftImaginary;
    float curLineWidth, filtLineWidth;
};
